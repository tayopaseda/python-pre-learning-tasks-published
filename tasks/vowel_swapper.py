def vowel_swapper(string):
    ans = ""
    for i in string: #iterate through string and add corresponding character to ans as conditions state
        if i == "a" or i== 'A':
            ans += '4'
        elif i == "e" or i == 'E':
            ans += '3'
        elif i == 'i' or i == 'I':
            ans += '!'
        elif i == 'o':
            ans += 'ooo'
        elif i == 'u' or i == 'U':
            ans += '|_|'
        elif i == "O":
            ans += '000'
        else:
            ans += i
            
    return ans
        

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console