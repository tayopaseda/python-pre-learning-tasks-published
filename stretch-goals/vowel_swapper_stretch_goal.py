def vowel_swapper(string):
    dic = {'a':0, 'e':0, 'i':0, 'o':0, 'u': 0} #dict keeps track of how many times we've seen vowel
    ans = ""
    for i in string: #iterate through string and add corresponding character to ans as conditions state
        if i == "a" or i== 'A': #if we see a vowel add to the count in dict 
            dic['a'] += 1
            if dic['a'] == 2: #if this is the second occurrence then swap as necessary
                ans += '4'
            else:     #if not then dont swap
                ans += i
        elif i == "e" or i == 'E':
            dic['e'] += 1
            if dic['e'] == 2:
                ans += '3'
            else:
                ans += i
        
        elif i == 'i' or i == 'I':
            dic['i'] += 1
            if dic['i'] == 2:
                ans += '!'
            else:
                ans += i
        elif i == 'o' or i == 'O':
            dic['o'] += 1
            if dic['o'] == 2:
                if i == 'o':
                    ans += 'ooo'
                else:
                    ans += '000'
            else:
                ans += i
        elif i == 'u' or i == 'U':
            dic['u'] += 1
            if dic['u'] == 2:
                ans += '|_|'
            else:
                ans += i

        else:
            ans += i
            
    return ans
    

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console
