def factors(number):
    if number == 1: #two and one have no factors
        return "One isnt a prime but it has no factors either"
    factors = []
    i = 2
    while i <= number//2: #checks all numbers from 2 till number//2 to check if its a factor
        if number % i == 0: #if its a factor add it to factors list and also add number / i (cuts operations in half)
            factors.append(i)
            factors.append(int(number/i))
        i+=1
    if factors:
        return list(set(factors)) #set removes duplicates
    else:
        return f"{number} is a prime number"

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
