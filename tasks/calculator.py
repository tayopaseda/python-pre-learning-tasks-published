def calculator(a, b, operator):
    if operator == '+': #addition
        return a + b
    elif operator == '-': #subtraction
        return a - b
    elif operator == '*': #multiplication
        return a * b
    elif operator == '/': #make sure we do not divide by 0 and a is divisible by b ? (since answer must be int)
        if b == 0:
            return "Cannot divide by Zero"
        elif a%b != 0:
            return f"{a} not divisible by {b}"
        else:
            return int(a / b)

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
