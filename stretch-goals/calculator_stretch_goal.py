def calculator(a, b, operator):
    ans  = 0
    if operator == '+': #addition
        ans = a + b
    elif operator == '-': #subtraction
        ans = a - b
    elif operator == '*': #multiplication
        ans = a * b
    elif operator == '/': #make sure we do not divide by 0 and a is divisible by b ? (since answer must be int)
        if b == 0:
            return "Cannot divide by Zero"
        elif a%b != 0:
            return f"{a} not divisible by {b}"
        else:
            ans = int(a / b)
            
    return f"{ans:0b}"

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
